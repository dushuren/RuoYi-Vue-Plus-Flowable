package com.ruoyi.workflow.flowable.cmd;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.common.engine.impl.interceptor.Command;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;

/**
 * @description: 校验流程变量
 * @author: gssong
 * @date: 2022/8/22 18:26
 */
public class ExpressCheckCmd implements Command<Boolean>, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ExpressCheckCmd.class);

    private final String conditionExpression;

    private final Map<String, Object> variableMap;

    public ExpressCheckCmd(String conditionExpression, Map<String, Object> variableMap) {
        this.conditionExpression = conditionExpression;
        this.variableMap = variableMap;
    }

    @Override
    public Boolean execute(CommandContext commandContext) {
        if (CollUtil.isEmpty(variableMap)) {
            return false;
        }
        ProcessEngineConfigurationImpl processEngineConfiguration = SpringUtils.getBean(ProcessEngineConfigurationImpl.class);
        Expression expression = processEngineConfiguration.getExpressionManager().createExpression(this.conditionExpression);
        DelegateExecution delegateExecution = new ExecutionEntityImpl();
        delegateExecution.setTransientVariables(variableMap);
        Object result;
        try {
            result = expression.getValue(delegateExecution);
        } catch (Exception e) {
            logger.error("流程变量不存在："+e.getMessage());
            return false;
        }
        return (Boolean) result;
    }

}
